This is a extension for [Nette](https://nette.org) framework that you can use for user authentication and registration.

Installation
------------

The best way to install Web Project is using Composer. If you don't have Composer yet,
download it following [the instructions](https://doc.nette.org/composer). Then use command:

	composer create-project nette/sandbox path/to/install
	cd path/to/install