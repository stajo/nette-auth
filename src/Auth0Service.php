<?php declare(strict_types=1);

namespace Stajo\NetteAuthExtension;
use Auth0\SDK\Auth0;
use Auth0\SDK\Configuration\SdkConfiguration;
use Auth0\SDK\Exception\ConfigurationException;
use Nette\Application\BadRequestException;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\Security\Identity;
use Nette\SmartObject;
use Tracy\Debugger;

class Auth0Service
{
    use SmartObject;

    /** @var Auth0 */
    private Auth0 $auth0;

    /** @var Cache */
    private $cache;

    /** @var array */
    private $credentials;


    /**
     * @throws ConfigurationException
     */
    public function __construct(array $credentials, IStorage $storage)
    {
        $this->credentials = $credentials;
        $this->cache = new Cache($storage, self::class);
        $configuration = new SdkConfiguration($credentials);
        $this->auth0 = new Auth0($configuration);
    }

    /**
     * @throws BadRequestException
     */
    public function getUser(): ?array
    {
        try {
            return $this->auth0->getUser();

        } catch (\Exception $exception) {
            Debugger::log($exception->getMessage());
            throw new BadRequestException();
        }
    }

    public function createIdentity($userId=null): Identity
    {
        if (!$userId) {
            try {
                $this->auth0->exchange();
                $user = $this->auth0->getUser();
                $userId = $user['sub'];

            } catch (\Throwable $exception) {
                Debugger::log($exception->getMessage());
            }
        }

        $user = $this->getUserById($userId);
        $roles = $user->app_metadata->roles ?? [];
        return new Identity($user->user_id, $roles, (array) $user);
    }

    /**
     * @param $email
     * @param $password
     * @param array $appData
     * @param array $userData
     * @return mixed
     * @throws BadRequestException
     * @throws \Throwable
     */
    public function createUser($email, $password, $appData=[], $userData=[])
    {
        $route = "/api/v2/users";
        $data = [
            'connection' => 'Username-Password-Authentication',
            'email' => $email,
            'password' => $password,
            'email_verified' => true,
            'app_metadata' => (object) $appData,
            'user_metadata' => (object) $userData
        ];

        $result = $this->createRequest($route, 'POST', $data, $this->getClientToken());
        if (isset($result->statusCode)) {
            Debugger::log($result->message);
            throw new BadRequestException();
        }

        return $result;
    }

    /**
     * @param $userId
     * @return mixed
     * @throws BadRequestException
     * @throws \Throwable
     */
    public function deleteUser($userId)
    {
        $route = "/api/v2/users/{$userId}";
        $result = $this->createRequest($route, 'DELETE', [], $this->getClientToken());
        if (isset($result->statusCode)) {
            Debugger::log($result->message);
            throw new BadRequestException();
        }

        return $result;
    }

    /**
     * @param $userId
     * @param $data
     * @return mixed
     * @throws BadRequestException
     * @throws \Throwable
     */
    public function editUser($userId, $data)
    {
        $route = "/api/v2/users/{$userId}";

        $result = $this->createRequest($route, 'PATCH', $data, $this->getClientToken());
        if (isset($result->statusCode)) {
            Debugger::log($result->message);
            throw new BadRequestException();
        }

        return $result;
    }

    /**
     * @throws BadRequestException
     */
    public function changePassword(string $email)
    {
        $route = "/dbconnections/change_password";

        $data = [
            'client_id' => $this->credentials['clientId'],
            'client_secret' => $this->credentials['clientSecret'],
            'connection' => 'Username-Password-Authentication',
            'email' => $email
        ];

        $result = $this->createRequest($route, 'POST', $data);
        if (isset($result->statusCode)) {
            Debugger::log($result->message);
            throw new BadRequestException();
        }

        return $result;
    }

    /**
     * @param $userId
     * @param $password
     * @return mixed
     * @throws BadRequestException
     * @throws \Throwable
     */
    public function editPassword($userId, $password)
    {
        $result = $this->editUser($userId, ['password' => $password]);
        return $result;
    }

    /**
     * @param $userId
     * @param $values
     * @return mixed
     * @throws BadRequestException
     * @throws \Throwable
     */
    public function editUserData($userId, $values)
    {
        $data['user_metadata'] = $values;
        return $this->editUser($userId, $data);
    }

    /**
     * @param $userId
     * @param $values
     * @return mixed
     * @throws BadRequestException
     * @throws \Throwable
     */
    public function editAppData($userId, $values)
    {
        $data['app_metadata'] = $values;
        return $this->editUser($userId, $data);
    }

    /**
     * @param $userId
     * @param array $roles
     * @return Identity
     * @throws BadRequestException
     * @throws \Throwable
     */
    public function editRoles($userId, array $roles)
    {
        $data['app_metadata']['roles'] = $roles;
        $user = $this->editUser($userId, $data);
        return new Identity($user->user_id, $user->app_metadata->roles, $user);
    }

    /**
     * @param null $criteria
     * @return array|mixed
     * @throws BadRequestException
     * @throws \Throwable
     */
    public function getUserList($criteria=null)
    {
        $route = '/api/v2/users';
        $result = $this->createRequest($route, 'GET', ['q'=>$criteria, 'per_page' => '100', 'search_engine'=>'v3'], $this->getClientToken());

        if (isset($result->message)) {
            Debugger::log($result->message);
            return [];
        }
        return $result;
    }

    /**
     * @param $email
     * @return mixed|null
     * @throws BadRequestException
     * @throws \Throwable
     */
    public function getUserByEmail($email)
    {
        $filter = self::createSearchString([$email], 'email');
        $users = $this->getUserList($filter);
        return $users[0]  ??  null;
    }

    /**
     * @param $userId
     * @return mixed|null
     * @throws BadRequestException
     * @throws \Throwable
     */
    public function getUserById($userId)
    {
        $route = "/api/v2/users/{$userId}";
        $result = $this->createRequest($route, 'GET', [], $this->getClientToken());
        if (isset($result->error)) {
            Debugger::log($result->message);
            return null;
        }

        return $result;
    }

    /**
     * @param $email
     * @param $password
     * @return mixed
     * @throws BadRequestException
     */
    public function getUserToken($email, $password)
    {
        $route = '/oauth/token';
        $data = [
            'grant_type' => 'password',
            'username' => $email,
            'password' => $password,
            'client_id' => $this->credentials['client_id'],
            'client_secret' => $this->credentials['client_secret']
        ];

        return $this->createRequest($route, 'POST', $data);
    }

    /**
     * @return mixed
     * @throws BadRequestException
     * @throws \Throwable
     */
    private function getClientToken()
    {
        $data = [
            'grant_type' => 'client_credentials',
            'client_id' => $this->credentials['clientId'],
            'client_secret' => $this->credentials['clientSecret'],
            'audience' => "https://{$this->credentials['domain']}/api/v2/"
        ];

        $token = $this->cache->load('token');
        if (!isset($token)) {
            $response = $this->createRequest('/oauth/token', 'POST', $data);
            if (isset($response->access_token)) {
                $token = $response->access_token;
                $this->cache->save('token', $token, [Cache::EXPIRE => '24 hours']);
            }
        }

        return $token;
    }

    /**
     * @param array $values
     * @param string $searchBy
     * @return string
     */
    public static function createSearchString($values, $searchBy = 'user_id')
    {
        $string = is_integer(reset($values))  ?  '%s:(%s)' : '%s:("%s")';
        $values = is_integer(reset($values))  ?  implode(' OR ', $values) : implode('" OR "', $values);
        return sprintf($string, $searchBy, $values);
    }

    /**
     * @return mixed
     * @throws BadRequestException
     */
    public function authorize()
    {
        $route = '/authorize';
        $data = [
            'response_type' => 'code',
            'client_id' => $this->credentials['clientId'],
            'redirect_uri' => $this->credentials['redirectUri'],
            'audience' => "https://{$this->credentials['domain']}/api/v2/"
        ];
        return $this->createRequest($route, 'GET', $data, null, true);
    }

    public function login()
    {
        $url = $this->auth0->login();
        header("Location: $url");
        die();
    }

    public function logout()
    {
        $url = $this->auth0->logout();
        header("Location: $url");
    }

    public function signUp()
    {
        $url = $this->auth0->signup();
        header("Location: $url");
        die();
    }

    /**
     * @param $route
     * @param string $method
     * @param array $data
     * @param null $token
     * @param bool $redirect
     * @return mixed
     * @throws BadRequestException
     */
    private function createRequest($route, $method='POST', $data=[], $token=null, $redirect=false)
    {
        $httpHeader = ['content-type: application/json'];
        if ($token) {
            $httpHeader[] = 'authorization: Bearer ' . $token;
        }

        $curl = curl_init();

        if (count($data)  &&  $method == 'GET') {
            $route .= '?';
            foreach ($data as $key => $value) {
                $escaped = curl_escape($curl, $value);
                $route .= "{$key}={$escaped}&";
            }
        }

        $options = [
            CURLOPT_URL => 'https://' . $this->credentials['domain'] . $route,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => $httpHeader
        ];

        curl_setopt_array($curl, $options);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        $redirectURL = curl_getinfo($curl,CURLINFO_EFFECTIVE_URL );
        curl_close($curl);

        if ($err) {
            Debugger::log($err);
            throw new BadRequestException('cURL Error #:' . $err);
        } elseif ($redirect) {
            header("Location: {$redirectURL}");
            die();
        } else {
            return json_decode($response);
        }
    }
}
