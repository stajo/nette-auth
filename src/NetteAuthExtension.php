<?php

namespace Stajo\NetteAuthExtension;
use Auth0\SDK\Auth0;
use Nette\DI\CompilerExtension;
use Nette\DI\Extensions\InjectExtension;

class NetteAuthExtension extends CompilerExtension
{
	/** @var array */
	private $defaults = [
		'socialButtonStyle' => 'small',
	];


	/**
	 * @inheritdoc
	 */
	public function loadConfiguration()
	{
		$builder = $this->getContainerBuilder();
		$config = $this->getConfig();

		$builder->addDefinition($this->prefix('auth0Service'))
			->setFactory(Auth0Service::class, [$config['credentials']])
            ->addTag(InjectExtension::TAG_INJECT);

		$builder->addDefinition($this->prefix('htmlWrapper'))
			->setFactory(HtmlWrapper::class, [$config['credentials'], $config['lockOptions']])
            ->addTag(InjectExtension::TAG_INJECT);

		$builder->addDefinition($this->prefix('auth0Sdk'))
			->setFactory(Auth0::class, [$config['credentials']]);
	}
}
