<?php

namespace Stajo\NetteAuthExtension;
use Nette\SmartObject;

class HtmlWrapper
{
	use SmartObject;

	/** @var array */
	private $credentials;

	/** @var array */
	private $lockOptions;


	/**
	 * @param array $credentials
	 * @param array $lockOptions
	 */
	public function __construct(array $credentials, array $lockOptions)
	{
		$this->credentials = $credentials;
		$this->lockOptions = $lockOptions;
	}

	/**
	 * @return array
	 */
	public function getLockOptions()
	{
		$lockOptions = [
			'language' => $this->getLocale(),
			'languageDictionary' => [
				'title' => ''
			],
			'auth' => [
				'redirectUrl' => $this->getRedirectUrl()
			]
		];

		if (isset($this->lockOptions['logo'])) {
			$lockOptions['theme']['logo'] = $this->lockOptions['logo'];
		}
		if (isset($this->lockOptions['socialButtonStyle'])) {
			$lockOptions['socialButtonStyle'] = $this->lockOptions['socialButtonStyle'];
		}
		if (isset($this->lockOptions['rememberLastLogin'])) {
			$lockOptions['rememberLastLogin'] = $this->lockOptions['rememberLastLogin'];
		}

		return $lockOptions;
	}

	/**
	 * @return string
	 */
	public function getLocale()
	{
		return 'sk';
	}

	/**
	 * @return string
	 */
	public function getRedirectUrl()
	{
		return $this->credentials['redirectUri'];
	}

	/**
	 * @return string
	 */
	public function getClientId()
	{
		return $this->credentials['clientId'];
	}

	/**
	 * @return string
	 */
	public function getDomain()
	{
		return $this->credentials['domain'];
	}
}
